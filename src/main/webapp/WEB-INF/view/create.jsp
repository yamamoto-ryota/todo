<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta charset="utf-8">
<title>新規タスク登録</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
</head>
<body>
  <div class="uk-container uk-container-center uk-margin-small-bottom" >
   <h1>新規タスク登録</h1>

       <a href="${pageContext.request.contextPath}/home/">未完了タスク一覧</a>
       <a href="${pageContext.request.contextPath}/finishedTask">完了済タスク一覧</a>

   <form:form modelAttribute="TaskForm">
      <form:errors path="*" /><br />

      <label for="title">タスク名</label><br />
      <form:input path="title" value="${title}" /><br />

      <label for="tag">タグ</label><br />
      <form:input path="tag" value="${tag}" /><br />

      <label for="due_date">締め切り</label><br />
      <form:input type="date" path="dueDate" /><br />

      <label for="comment">コメント</label><br />
      <form:textarea cols="20" rows="10" path="comment" /><br />

      <input type="submit" value="登録">

   </form:form>
  </div>
</body>
</html>