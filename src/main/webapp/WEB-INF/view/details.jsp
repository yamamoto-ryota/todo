<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>タスク詳細</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
</head>
<body>
   <div class="uk-container uk-container-center uk-margin-small-bottom" >
    <h1>タスク詳細</h1>
    <c:if test="${task.isFinished == 0}">
       <a href="${pageContext.request.contextPath}/home/">未完了タスク一覧</a>
    </c:if>
    <c:if test="${task.isFinished == 1}">
       <a href="${pageContext.request.contextPath}/finishedTask">完了済タスク一覧</a>
    </c:if>


    <form:form modelAttribute="TaskForm" action="/ToDo/update/${task.id}">
    <form:errors path="*" /><br />

        <label for="title">タスク名</label><br />
        <input name="title" id="title" value="${task.title}" /><br />

        <label for="tag">タグ</label><br />
        <input name="tag" id="tag" value="${task.tag}" /><br />

        <label for="due_date">締め切り</label><br />
        <input type="date" name="dueDate" id="dueDate" value="${task.dueDate}"/><br />

        <label for="is_finished">状態</label><br />
        <c:if test="${task.isFinished == 0}">
           <select name="isFinished">
              <option value="0" selected >
                 <c:out value="未完了" />
              </option>
              <option value="1">
                 <c:out value="完了" />
              </option>
           </select><br />
        </c:if>

         <c:if test="${task.isFinished == 1}">
            <select name="isFinished">
               <option value="0">
                  <c:out value="未完了" />
               </option>
               <option value="1" selected>
                  <c:out value="完了" />
               </option>
            </select><br />
        </c:if>

        <label for="comment">コメント</label><br />
        <textarea cols="20" rows="10" name="comment" id="comment" >${task.comment}</textarea><br />


        <input type="submit" value="更新">

    </form:form>
   </div>
</body>
</html>