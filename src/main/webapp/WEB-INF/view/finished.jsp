<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>完了済タスク一覧</title>
        <script type="text/javascript">

           function deleteCheck(){
	         if(window.confirm('削除します。よろしいですか？')){
		       return true;
	         }else{
		       return false;
	         }
           }

        </script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
    </head>
    <body>
      <div class="uk-container uk-container-center uk-margin-small-bottom" >
        <h1>${message1}</h1>
        <a href="${pageContext.request.contextPath}/create">タスク登録</a>
        <a href="${pageContext.request.contextPath}/home/">未完了タスク一覧</a>

        <div class="search">
          <form:form moddelAttribute="TaskForm" action="/ToDo/search/finished" method="get">

            <label for="tag">タグ</label>
            <input name="tag" id = "tag" value="${tag}"/>

            <input type="submit" value="絞込み" /><br />
          </form:form>
        </div>

        <c:forEach items="${tasks}" var="task">
          <c:if test="${task.isFinished == 1}">
           <div class="uk-section">

            <c:out value="タスク名：${task.title}"></c:out><br />
            <c:out value="タグ：${task.tag}"></c:out><br />
            <c:if test="${task.dueDate == null}">
               <c:out value="締め切り：未登録"></c:out>
            </c:if>
            <c:if test="${task.dueDate != null}">
               <c:out value="締め切り：${task.dueDate}"></c:out>
            </c:if>
           <div class="uk-grid">
            <form:form modelAttribute="TaskForm" action="/ToDo/delete/" onSubmit="return deleteCheck()" >
               <input type="hidden" name="id" value="${task.id}"  />
               <input type="submit" value="削除">
            </form:form>

            <form:form modelAttribute="TaskForm" action="/ToDo/update/${task.id}" method="get" >
               <input type="hidden" name="id" value="${task.id}"  />
               <input type="submit" value="編集">
            </form:form>
           </div>
           </div>
          </c:if>
        </c:forEach>
      </div>
    </body>
</html>